function openChat() {
    document.getElementById("myChat").style.display = "block";
}

function closeChat() {
    document.getElementById("myChat").style.display = "none";
}

function sendMessage() {
    var input = document.getElementById("chatInput");
    var message = input.value;
    if (message.trim() !== "") {
        var chatBody = document.querySelector(".chat-body");
        var newMessage = document.createElement("p");
        newMessage.textContent = message;
        chatBody.appendChild(newMessage);
        input.value = "";
        chatBody.scrollTop = chatBody.scrollHeight;
    }
}

async function getallrecords() {
    const response = await fetch('http://localhost:3000/barf-rag/');
    const data = await response.json();
    console.log(data);
    return data;
}

getallrecords()